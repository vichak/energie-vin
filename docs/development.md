# Installation

```bash
$ npm install
$ docker compose up -d
```

# Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

http://localhost:3000

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## inside docker compose

  * postgres you can browse it using adminer at http://localhost:8081 root/root
  * mailpit as local mailbox at http://localhost:28025
  * keycloak at http://localhost:28080 admin/keycloak

## import wines from external service

```bash
curl --location 'localhost:3000/wine-source/populate'
```