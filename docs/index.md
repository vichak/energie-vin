# Energie vin

Un porteur de projet vous recrute en tant que premier développeur back-end dans sa jeune équipe.

**Son pitch**

Concevoir une plateforme de recherche référençant les vins vendus sur des sites spécialisés.

  * Les données (propriétés de la bouteille, prix et site de vente) pourront être récupérées à partir d’un service externe dont on ne connait pas encore les spécificités.
  * Pour se démarquer de la concurrence, la plateforme veut faire la part belle aux experts du domaine avec la possibilité d’intégrer leurs notes de dégustation.
  * La fonctionnalité à forte valeur serait de fournir une évaluation de chacune des bouteilles, basée sur la moyenne des notes attribuées par ces experts.
  * Autres fonctionnalités à fort potentiel :
    * Un système d’alerting où les utilisateurs peuvent sauvegarder leurs recherches et être notifiés si une bouteille nouvellement identifiée correspond.
    * L’accès à l’historisation des prix pour chaque bouteille pour comprendre la tendance.

Quand vous lui parlez d’un MVP, le porteur de projet aimerait pouvoir récupérer facilement les vins vendus, classés par meilleure moyenne des notes de dégustation, dont le prix est compris dans l’intervalle fixé par l’utilisateur.

Vous avez rendez-vous dans quelques jours avec lui. Son souhait, est que vous puissiez mettre en place une première structure du projet avec une prémisse de développement afin que vous et les prochains développeurs puissiez travailler dans de bonnes conditions.
Vous vous engagez à fournir un premier livrable récupérable via un repo git et si besoin, tout document utile à la conception. Vous discuterez avec lui de la stratégie future : des prochains développements, de l’infrastructure et des nouveaux recrutements nécessaires à la bonne réalisation de ce projet ambitieux.