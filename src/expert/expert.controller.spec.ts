import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Expert } from './entities/expert.entity';
import { ExpertController } from './expert.controller';
import { ExpertService } from './expert.service';

describe('ExpertController', () => {
  let controller: ExpertController;
  const mockRepositoryExpert = {
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({});
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExpertController],
      providers: [
        ExpertService,
        {
          provide: getRepositoryToken(Expert),
          useValue: mockRepositoryExpert,
        },
      ],
    }).compile();

    controller = module.get<ExpertController>(ExpertController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
