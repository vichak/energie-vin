import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Expert } from './entities/expert.entity';
import { ExpertController } from './expert.controller';
import { ExpertService } from './expert.service';

@Module({
  imports: [TypeOrmModule.forFeature([Expert])],
  controllers: [ExpertController],
  providers: [ExpertService],
})
export class ExpertModule {}
