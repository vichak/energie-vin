import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Expert } from './entities/expert.entity';
import { ExpertService } from './expert.service';

describe('ExpertService', () => {
  let service: ExpertService;
  const mockRepositoryExpert = {
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({});
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ExpertService,
        {
          provide: getRepositoryToken(Expert),
          useValue: mockRepositoryExpert,
        },
      ],
    }).compile();

    service = module.get<ExpertService>(ExpertService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
