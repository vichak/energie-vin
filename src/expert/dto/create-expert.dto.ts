import { ApiProperty } from '@nestjs/swagger';

export class CreateExpertDto {
  @ApiProperty()
  name: string;
}
