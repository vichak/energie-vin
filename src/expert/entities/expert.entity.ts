import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Expert {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 300 })
  name: string;
}
