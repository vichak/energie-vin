import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateExpertDto } from './dto/create-expert.dto';
import { UpdateExpertDto } from './dto/update-expert.dto';
import { Expert } from './entities/expert.entity';

@Injectable()
export class ExpertService {
  constructor(
    @InjectRepository(Expert) private readonly repository: Repository<Expert>,
  ) {}

  create(createExpertDto: CreateExpertDto): Promise<Expert> {
    const expert: Expert = new Expert();
    expert.name = createExpertDto.name;
    return this.repository.save(expert);
  }

  findAll(): Promise<Expert[]> {
    return this.repository.find();
  }

  findOne(id: number): Promise<Expert> {
    return this.repository.findOneBy({ id });
  }

  update(id: number, updateExpertDto: UpdateExpertDto) {
    const expert: Expert = new Expert();
    expert.name = updateExpertDto.name;
    expert.id = id;
    return this.repository.save(expert);
  }

  remove(id: number): Promise<{ affected?: number }> {
    return this.repository.delete(id);
  }
}
