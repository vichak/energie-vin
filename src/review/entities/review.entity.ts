import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Expert } from '../../expert/entities/expert.entity';
import { Wine } from '../../wine/entities/wine.entity';

@Entity()
export class Review {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'integer' })
  note: number;

  @Column({ type: 'varchar', length: 300 })
  comment: string;

  @ManyToOne(() => Wine)
  @JoinColumn()
  wine: Wine;

  @OneToOne(() => Expert)
  @JoinColumn()
  author: Expert;
}
