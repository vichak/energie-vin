import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Expert } from '../expert/entities/expert.entity';
import { Wine } from '../wine/entities/wine.entity';
import { CreateReviewDto } from './dto/create-review.dto';
import { UpdateReviewDto } from './dto/update-review.dto';
import { Review } from './entities/review.entity';

@Injectable()
export class ReviewService {
  constructor(
    @InjectRepository(Review) private readonly repository: Repository<Review>,
  ) {}

  create(createReviewDto: CreateReviewDto): Promise<Review> {
    const review: Review = new Review();
    review.note = createReviewDto.note;
    review.comment = createReviewDto.comment;
    review.author = new Expert();
    review.author.id = createReviewDto.authorId;
    review.wine = new Wine();
    review.wine.id = createReviewDto.wineId;
    return this.repository.save(review);
  }

  findAll(): Promise<Review[]> {
    return this.repository.find({ relations: { author: true, wine: true } });
  }

  findOne(id: number): Promise<Review> {
    return this.repository.findOneBy({ id });
  }

  update(id: number, updateReviewDto: UpdateReviewDto) {
    const review: Review = new Review();
    review.note = updateReviewDto.note;
    review.comment = updateReviewDto.comment;
    review.author.id = updateReviewDto.authorId;
    review.wine.id = updateReviewDto.wineId;
    review.id = id;
    return this.repository.save(review);
  }

  remove(id: number): Promise<{ affected?: number }> {
    return this.repository.delete(id);
  }

  findAllByWine(wineId: number): Promise<Review[]> {
    return this.repository.find({
      where: { wine: { id: wineId } },
    });
  }
}
