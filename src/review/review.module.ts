import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WineModule } from '../wine/wine.module';
import { Review } from './entities/review.entity';
import { ReviewController } from './review.controller';
import { ReviewService } from './review.service';

@Module({
  imports: [TypeOrmModule.forFeature([Review]), WineModule],
  controllers: [ReviewController],
  providers: [ReviewService],
})
export class ReviewModule {}
