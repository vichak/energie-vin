import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Wine } from '../wine/entities/wine.entity';
import { WineService } from '../wine/wine.service';
import { Review } from './entities/review.entity';
import { ReviewController } from './review.controller';
import { ReviewService } from './review.service';

describe('ReviewController', () => {
  let controller: ReviewController;
  const mockRepositoryReview = {
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({});
    }),
  };
  const mockRepositoryWine = {
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({});
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReviewController],
      providers: [
        ReviewService,
        {
          provide: getRepositoryToken(Review),
          useValue: mockRepositoryReview,
        },
        WineService,
        {
          provide: getRepositoryToken(Wine),
          useValue: mockRepositoryWine,
        },
      ],
    }).compile();

    controller = module.get<ReviewController>(ReviewController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
