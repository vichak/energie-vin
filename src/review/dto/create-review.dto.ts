import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateReviewDto {
  @ApiProperty({ required: true, description: "Note de l'avis" })
  @IsNotEmpty()
  note: number;

  @ApiProperty({ description: "Commentaire de l'avis" })
  @IsString()
  comment: string;

  @ApiProperty({ required: true, description: 'id du vin' })
  wineId: number;

  @ApiProperty({ required: true, description: "id de l'expert" })
  authorId: number;
}
