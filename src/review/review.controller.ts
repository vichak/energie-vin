import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ReviewService } from './review.service';
import { CreateReviewDto } from './dto/create-review.dto';
import { UpdateReviewDto } from './dto/update-review.dto';
import { WineService } from '../wine/wine.service';
import { Review } from './entities/review.entity';

@Controller('review')
export class ReviewController {
  constructor(
    private readonly reviewService: ReviewService,
    private readonly wineService: WineService,
  ) {}

  @Post()
  async create(@Body() createReviewDto: CreateReviewDto) {
    const review = await this.reviewService.create(createReviewDto);
    await this.updateWineReviewNote(createReviewDto.wineId);
    return review;
  }

  @Get()
  findAll() {
    return this.reviewService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reviewService.findOne(+id);
  }

  @Patch(':id')
  async update(
    @Param('id') id: number,
    @Body() updateReviewDto: UpdateReviewDto,
  ) {
    const review = await this.reviewService.update(id, updateReviewDto);
    await this.updateWineReviewNote(updateReviewDto.wineId);
    return review;
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reviewService.remove(+id);
  }

  private async updateWineReviewNote(wineId: number) {
    const reviews: Review[] = await this.reviewService.findAllByWine(wineId);
    this.wineService.updateWineWithReviews(wineId, reviews);
  }
}
