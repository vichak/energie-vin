import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Wine } from './entities/wine.entity';
import { WineController } from './wine.controller';
import { WineService } from './wine.service';

@Module({
  imports: [TypeOrmModule.forFeature([Wine])],
  controllers: [WineController],
  providers: [WineService],
  exports: [WineService],
})
export class WineModule {}
