import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Wine {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'integer', nullable: true })
  externalId: number;

  @Column({ type: 'varchar', length: 300 })
  name: string;

  @Column({ type: 'integer' })
  price: number;

  @Column({ type: 'varchar', length: 300 })
  salesSite: string;

  @Column({ type: 'integer', nullable: true })
  expertNote: number;
}
