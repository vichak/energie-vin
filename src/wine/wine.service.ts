import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, LessThan, MoreThan, Repository } from 'typeorm';
import { Review } from '../review/entities/review.entity';
import { WineDto } from '../wine-source/dto/wine.dto';
import { CreateWineDto } from './dto/create-wine.dto';
import { UpdateWineDto } from './dto/update-wine.dto';
import { Wine } from './entities/wine.entity';

@Injectable()
export class WineService {
  private readonly logger = new Logger(WineService.name);

  constructor(
    @InjectRepository(Wine) private readonly repository: Repository<Wine>,
  ) {}

  create(createWineDto: CreateWineDto): Promise<Wine> {
    const wine: Wine = new Wine();
    wine.name = createWineDto.name;
    wine.price = createWineDto.price;
    wine.salesSite = createWineDto.salesSite;
    return this.repository.save(wine);
  }

  findAll(): Promise<Wine[]> {
    return this.repository.find();
  }

  findOne(id: number): Promise<Wine> {
    return this.repository.findOneBy({ id });
  }

  update(id: number, updateWineDto: UpdateWineDto) {
    const wine: Wine = new Wine();
    wine.name = updateWineDto.name;
    wine.price = updateWineDto.price;
    wine.salesSite = updateWineDto.salesSite;
    wine.id = id;
    return this.repository.save(wine);
  }

  remove(id: number): Promise<{ affected?: number }> {
    return this.repository.delete(id);
  }

  async updateWineWithReviews(
    wineId: number,
    reviews: Review[],
  ): Promise<Wine> {
    const wine = await this.repository.findOneBy({ id: wineId });
    const sum = reviews.map((review) => review.note).reduce((a, b) => a + b, 0);
    const avg = sum / reviews.length || 0;
    wine.expertNote = avg;
    this.logger.log(
      `wine : ${wineId} got new expert note : ${wine.expertNote}`,
    );
    return this.repository.save(wine);
  }

  async updateWithWineSource(wineDtos: WineDto[]): Promise<Wine[]> {
    const wineIds = wineDtos.map((wine) => wine.id);
    const wines = await this.repository.findBy({
      id: In(wineIds),
    });
    wineDtos.forEach((wineDto) => {
      const i = wines.findIndex((w) => w.externalId === wineDto.id);
      if (i >= 0) {
        // it's already in database
        const w = wines.at(i);
        w.name = wineDto.name;
        w.salesSite = wineDto.salesSite;
        w.price = wineDto.price;
        // TODO historise price changes
      } else {
        const newWine = new Wine();
        newWine.externalId = wineDto.id;
        newWine.price = wineDto.price;
        newWine.name = wineDto.name;
        newWine.salesSite = wineDto.salesSite;
        wines.push(newWine);
      }
    });
    return this.repository.save(wines);
  }

  /**
   *
   * @param
   * @returns Quand vous lui parlez d’un MVP, le porteur de projet aimerait pouvoir récupérer facilement les vins vendus, classés par meilleure moyenne des notes de dégustation, dont le prix est compris dans l’intervalle fixé par l’utilisateur.
   */
  findMvp(priceStart: number, priceEnd: number): Promise<Wine[]> {
    this.logger.log(
      `find mvp with priceStart:${priceStart} and priceEnd:${priceEnd}`,
    );
    return this.repository.find({
      order: {
        expertNote: 'ASC',
      },
      where: [{ price: MoreThan(priceStart) }, { price: LessThan(priceEnd) }],
    });
  }
}
