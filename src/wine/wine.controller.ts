import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import { WineService } from './wine.service';
import { CreateWineDto } from './dto/create-wine.dto';
import { UpdateWineDto } from './dto/update-wine.dto';
import { ApiOperation } from '@nestjs/swagger';
import { Wine } from './entities/wine.entity';

@Controller('wine')
export class WineController {
  constructor(private readonly wineService: WineService) {}

  @Post()
  @ApiOperation({ summary: 'Create wine' })
  create(@Body() createWineDto: CreateWineDto) {
    return this.wineService.create(createWineDto);
  }

  @Get()
  @ApiOperation({ summary: 'Find all wines' })
  findAll() {
    return this.wineService.findAll();
  }

  @Get('mvp')
  @ApiOperation({ summary: 'Find wines for MVP' })
  findMvp(
    @Query('priceStart', ParseIntPipe) priceStart: number,
    @Query('priceEnd', ParseIntPipe) priceEnd: number,
  ): Promise<Wine[]> {
    return this.wineService.findMvp(priceStart, priceEnd);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find a wine by id' })
  findOne(@Param('id') id: string) {
    return this.wineService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update a wine' })
  update(@Param('id') id: string, @Body() updateWineDto: UpdateWineDto) {
    return this.wineService.update(+id, updateWineDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a wine' })
  remove(@Param('id') id: string) {
    return this.wineService.remove(+id);
  }
}
