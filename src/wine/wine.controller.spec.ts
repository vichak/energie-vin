import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Wine } from './entities/wine.entity';
import { WineController } from './wine.controller';
import { WineService } from './wine.service';

describe('WineController', () => {
  let controller: WineController;
  const mockRepositoryWine = {
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({});
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WineController],
      providers: [
        WineService,
        {
          provide: getRepositoryToken(Wine),
          useValue: mockRepositoryWine,
        },
      ],
    }).compile();

    controller = module.get<WineController>(WineController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
