import { ApiProperty } from '@nestjs/swagger';
import { IsUrl, Max, Min } from 'class-validator';

export class CreateWineDto {
  @ApiProperty({ description: 'Wine external source id' })
  externlaId: number;

  @ApiProperty({ description: 'Wine name' })
  name: string;

  @ApiProperty({ description: 'Wine price' })
  price: number;

  @ApiProperty({ description: 'Wine sales site' })
  @IsUrl()
  salesSite: string;

  @ApiProperty({ description: 'Wine expert note' })
  @Min(0)
  @Max(100)
  expertNote: number;
}
