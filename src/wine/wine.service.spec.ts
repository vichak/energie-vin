import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Wine } from './entities/wine.entity';
import { WineService } from './wine.service';

describe('WineService', () => {
  let service: WineService;
  const mockRepositoryWine = {
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({});
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WineService,
        {
          provide: getRepositoryToken(Wine),
          useValue: mockRepositoryWine,
        },
      ],
    }).compile();

    service = module.get<WineService>(WineService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
