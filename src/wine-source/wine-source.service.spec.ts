import { Test, TestingModule } from '@nestjs/testing';
import { WineSourceService } from './wine-source.service';

describe('WineSourceService', () => {
  let service: WineSourceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WineSourceService],
    }).compile();

    service = module.get<WineSourceService>(WineSourceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
