import { Controller, Get, Logger } from '@nestjs/common';
import { WineService } from '../wine/wine.service';
import { WineSourceService } from './wine-source.service';

@Controller('wine-source')
export class WineSourceController {
  private readonly logger = new Logger(WineSourceController.name);

  constructor(
    private readonly wineSourceService: WineSourceService,
    private readonly wineService: WineService,
  ) {}

  @Get('populate')
  populateWine(): string {
    this.logger.log('Populating wines with wine source');
    const wines = this.wineSourceService.getWines();
    this.wineService.updateWithWineSource(wines);
    return 'OK';
  }
}
