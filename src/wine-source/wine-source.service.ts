import { Injectable } from '@nestjs/common';
import { WineDto } from './dto/wine.dto';

/**
 * External service that retrive wine information
 */
@Injectable()
export class WineSourceService {
  wines = [
    {
      id: 1,
      name: 'Bordeaux',
      price: 10,
      salesSite: 'wineCash',
    },
    {
      id: 2,
      name: 'Bourgone',
      price: 20,
      salesSite: 'wineCash',
    },
    {
      id: 3,
      name: 'Loire',
      price: 3,
      salesSite: 'wineFactory',
    },
    {
      id: 4,
      name: 'Languedoc',
      price: 15,
      salesSite: 'wineCash',
    },
  ];
  getWines(): WineDto[] {
    return this.wines;
  }
}
