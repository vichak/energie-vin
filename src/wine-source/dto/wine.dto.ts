export class WineDto {
  id: number;
  name: string;
  price: number;
  salesSite: string;
}
