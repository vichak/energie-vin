import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Wine } from '../wine/entities/wine.entity';
import { WineService } from '../wine/wine.service';
import { WineSourceController } from './wine-source.controller';
import { WineSourceService } from './wine-source.service';

describe('WineSourceController', () => {
  let controller: WineSourceController;
  const mockRepositoryWine = {
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({});
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WineSourceController],
      providers: [
        WineSourceService,
        WineService,
        {
          provide: getRepositoryToken(Wine),
          useValue: mockRepositoryWine,
        },
      ],
    }).compile();

    controller = module.get<WineSourceController>(WineSourceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
