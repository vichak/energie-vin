import { MigrationInterface, QueryRunner } from 'typeorm';

/**
 * TODO update this migration because not up to date due to sync param on typeorm config
 */
export class Init1698933890064 implements MigrationInterface {
  async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
CREATE SEQUENCE expert_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."expert" (
    "id" integer DEFAULT nextval('expert_id_seq') NOT NULL,
    "name" character varying(300) NOT NULL,
    CONSTRAINT "PK_0062630832658e718267ce2941f" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE SEQUENCE wine_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."wine" (
    "id" integer DEFAULT nextval('wine_id_seq') NOT NULL,
    "name" character varying(300) NOT NULL,
    "price" integer NOT NULL,
    "salesSite" character varying(300) NOT NULL,
    "expertNote" integer,
    CONSTRAINT "wine_pkey" PRIMARY KEY ("id")
) WITH (oids = false);
`);
  }
  async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
DROP TABLE IF EXISTS "expert";
DROP SEQUENCE IF EXISTS expert_id_seq;
DROP TABLE IF EXISTS "wine";
DROP SEQUENCE IF EXISTS wine_id_seq;
    `);
  }
}
